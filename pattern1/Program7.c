/*If possible take no of rows from the user
 * 1 2 9 4
 * 25 6 49 8
 * 81 10 121 12 
 * 169 14 225 16
 */

#include<stdio.h>

void main(){
	int rows,i,j,num=1;

	printf("Enter number of rows::");
	scanf("%d",&rows);

	for(i=1;i<=rows;i++){
		for(j=1;j<=4;j++){
			if(num%2!=0){
				printf("%d\t",num*num);
				num++;
			}else{
				printf("%d\t",num);
				num++;
			}
		}
		printf("\n");
	}
}
