/*5. If possible take no of rows from the user
 * A B C D
 * B C D E
 * C D E F
 * D E F G
 */

#include<stdio.h>

void main(){
	int rows,i,j,x;

	printf("Enter rows::");
	scanf("%d",&rows);

	for(i=1;i<=rows;i++){
		x=64+i;
		for(j=1;j<=4;j++){
			printf("%c\t",x);
			x++;
		}
		printf("\n");
	}

}
