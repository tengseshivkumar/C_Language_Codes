/*
= = = = = =
$ $ $ $ $ $
@ @ @ @ @ @
= = = = = =
$ $ $ $ $ $
@ @ @ @ @ @
*/
#include<stdio.h>

void main(){
	int i,j,row,rn;
	printf("Row::");
	scanf("%d",&row);

	for(i=1;i<=row;i++){
		rn=i;
		while(rn>=3){
			rn=rn-3;
		}
		for(j=1;j<=6;j++){
			if(rn==1){
				printf("=\t");
			}else if(rn==2){
				printf("$\t");
			}else{
				printf("@\t");
			}
		}
		printf("\n");
	}

}
