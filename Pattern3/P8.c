/*16 15 14 13
 *L  K  J  I
 *8  7  6  5
 *D  C  B  A
 */

#include<stdio.h>

void main(){

	int i,j,row,ch,x;
	printf("Rows::");
	scanf("%d",&row);
	
	x=row*row;
	ch=64+x;	
	for(i=1;i<=row;i++){
		for(j=1;j<=row;j++){
			if(i%2==1){
				printf("%d\t",x);
			}else{
				printf("%c\t",ch);
			}
			ch--;
			x--;
		}
		printf("\n");	
	}
}
