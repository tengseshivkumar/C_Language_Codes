/*10. take no of rows from the user
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/

#include<stdio.h>

void main(){
	int i,j,rw,k;
	char ch;
	printf("Rows::");
	scanf("%d",&rw);
	for(i=1;i<=rw;i++){
		ch=64+rw;k=rw;
		for(j=1;j<=rw;j++){
			if(i%2==0){
				printf("%c%d\t",ch-k+1,j);
				k--;
			}else{
				printf("%c%d\t",ch,k);
				ch--;
				k--;
			}
		}
		printf("\n");
	}

}
