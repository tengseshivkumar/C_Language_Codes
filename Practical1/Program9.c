//WAP to print count of divisors of entered number

#include<stdio.h>

void main(){
	int num,count=0;
	printf("Enter Number:");
	scanf("%d",&num);
	if(num>0){
		for(int i=1;i<=num;i++){
	
			if(num%i==0){
				printf("%d is divisible by %d\n",num,i);
				count++;
			}	
		}
		printf("Count of Divisors of %d is=%d\n",num,count);
	}else{
		for(int i=-num;i>=1;i--){
			if(num%i==0){
				printf("%d is divisible by %d\n",num,i);
				count++;	
			}
		
		}
		printf("Count of Divisors of %d is=%d\n",num,count);
	}	
}
