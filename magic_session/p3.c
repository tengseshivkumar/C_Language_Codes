//WAP to print all composite number between given range;

#include<stdio.h>

void main(){
	int i,j,n1,n2,temp=0;
	printf("Enter start:");
	scanf("%d",&n1);
	printf("Enter end:");
	scanf("%d",&n2);
	
	for(i=n1;i<=n2;i++){
		temp=0;
		for(j=2;j<i;j++){
			if(i%j==0){
				temp=1;
			}
		}
		if(temp==1){
			printf("%d\t",i);
		}
	
	}
}
