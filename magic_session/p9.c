/*- - - 1
 *- - 1 2
 *- 1 2 3
 *1 2 3 4
 */
#include<stdio.h>
void main(){
	int i,j,sp,row;
	printf("Enter rows::");
	scanf("%d",&row);

	for(i=1;i<=row;i++){
		for(sp=row-1;sp>=i;sp--){
			printf("-\t");
		}
		int x=1;
		for(j=1;j<=i;j++){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
	}
}
