/*D e F g
 *e D c B 
 *F g H i
 *g F e D
 */

#include<stdio.h>

void main(){
	int i,j,row,ch,var;
	printf("Rows::");
	scanf("%d",&row);
	
	ch=64+row;
	for(i=1;i<=row;i++){
		var=ch;
		for(j=1;j<=row;j++){
			if(i%2!=0){
				if(j%2!=0){
					printf("%c\t",var);
				}else{
					printf("%c\t",var+32);
				}
				var++;
			}else{
				if(j%2!=0){
					printf("%c\t",var+32);
				}else{
					printf("%c\t",var);
				}
				var--;
			}
		}
		ch++;
		printf("\n");
	}
}
