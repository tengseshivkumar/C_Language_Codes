/*D D D D
 *  C C C
 *    B B 
 *      A 
 */
#include<stdio.h>

void main(){
	int i,j,row,ch;
	printf("Rows::");
	scanf("%d",&row);
	
	ch=64+row;
	for(i=1;i<=row;i++){
		for(j=1;j<i;j++){
			printf("-\t");
		}
		
		for(j=row;j>=i;j--){
			printf("%c\t",ch);
		}
		ch--;
		printf("\n");
	}
}
