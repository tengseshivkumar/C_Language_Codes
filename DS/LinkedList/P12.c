#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node{
	int data;
	struct Node *next;
}Nd;

void main(){
	
	Nd *head = NULL;

	Nd *newNode=(Nd*)malloc(sizeof(Nd));

	//n1
	newNode->data=10;
	newNode->next=NULL;
	
	//head pers...
	head=newNode;

	newNode=(Nd*)malloc(sizeof(Nd));
	//n2
	newNode->data=20;
	newNode->next=NULL;
	//2
	head->next=newNode;
	
	newNode=(Nd*)malloc(sizeof(Nd));

	//n3
	newNode->data=30;
	newNode->next=NULL;
	//3
	head->next->next=newNode;
	
	Nd *temp=head;
	
	while(temp!=NULL){
		printf("%d\t",temp->data);
		temp=temp->next;
	}	
}
