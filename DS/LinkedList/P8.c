#include<stdio.h>
#include<string.h>

typedef struct Employee{
	int empId;
	char eN[20];
	float sal;
	struct Employee *next;
}emp;

void main(){
	emp obj1,obj2,obj3;

	emp *head =&obj1;
	obj1.empId=1;
	strcpy(obj1.eN,"Kanha");
	obj1.sal=50.00;
	obj1.next=&obj2;
	
	obj2.empId=2;
	strcpy(obj2.eN,"Kanha");
	obj2.sal=50.00;
	obj2.next=&obj3;
	
	obj3.empId=3;
	strcpy(obj3.eN,"Kanha");
	obj3.sal=50.00;
	obj3.next=NULL;
	
	printf("To access the 1st object:\n");
	printf("%d\n",head->empId);	
	printf("%s\n",head->eN);
	printf("%f\n",head->sal);
	
	
	printf("To access the 1st object:\n");
	printf("%d\n",obj1.next->empId);
	printf("%s\n",obj1.next->eN);
	printf("%f\n",obj1.next->sal);
	
	printf("To access the 2nd object:\n");
	printf("%d\n",obj2.next->empId);
	printf("%s\n",obj2.next->eN);
	printf("%f\n",obj2.next->sal);
	

}
