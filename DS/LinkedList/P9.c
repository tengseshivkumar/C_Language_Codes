#include<stdio.h>
#include<string.h>

typedef struct Batsman{
	int jNo;
	char name[20];
	float avg;
	struct Batsman *next;
}bm;

void main(){
	bm obj1,obj2,obj3;

	bm *head = &obj1;
	head->jNo=10;
	strcpy(head->name,"Sachin");
	head->avg=70.78;
	head->next=&obj2;

	obj1.next->jNo=7;
	strcpy(obj1.next->name,"MSD");
	obj1.next->avg=80.88;
	obj1.next->next=&obj3;
	
	obj2.next->jNo=0;
	strcpy(obj2.next->name,"KLR");
	obj2.next->avg=00;
	obj2.next->next=NULL;

	
	printf("%d\n",head->jNo);
	printf("%s\n",head->name);
	printf("%f\n",head->avg);
	printf("%p\n",head->next);
	
	printf("%d\n",obj1.next->jNo);
	printf("%s\n",obj1.next->name);
	printf("%f\n",obj1.next->avg);
	printf("%p\n",obj1.next->next);
	
	printf("%d\n",obj2.next->jNo);
	printf("%s\n",obj2.next->name);
	printf("%f\n",obj2.next->avg);
	printf("%p\n",obj2.next->next);

}
