#include<stdio.h>

void main(){
	char carr[5]={'A','B','C','D','E'};

	char *cptr = carr;
	int *iptr = carr;

	cptr++;//increment by 1 byte
	iptr++;//increment by 4 byte.Here one warning will occur.

	printf("%c\n",*cptr);//B
	printf("%c\n",*iptr);//E
}
