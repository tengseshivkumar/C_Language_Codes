#include<stdio.h>
#include<string.h>

typedef struct Company{
	int emp;
	char name[20];
	float rev;
	struct Company *next;
}emp;

void main(){
	emp obj1,obj2,obj3;

	emp *head = &obj1;
	head->emp=10;
	strcpy(head->name,"Sachin");
	head->rev=70.78;
	head->next=&obj2;

	head->next->emp=7;
	strcpy(head->next->name,"MSD");
	head->next->rev=80.88;
	head->next->next=&obj3;
	
	head->next->next->emp=0;
	strcpy(head->next->next->name,"KLR");
	head->next->next->rev=00;
	head->next->next->next=NULL;

	
	printf("%d\n",head->emp);
	printf("%s\n",head->name);
	printf("%f\n",head->rev);
	printf("%p\n",head->next);
	
	printf("%d\n",head->next->emp);
	printf("%s\n",head->next->name);
	printf("%f\n",head->next->rev);
	printf("%p\n",head->next->next);
	
	printf("%d\n",head->next->next->emp);
	printf("%s\n",head->next->next->name);
	printf("%f\n",head->next->next->rev);
	printf("%p\n",head->next->next->next);

}
