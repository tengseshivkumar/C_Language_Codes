#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Employee{
	int eId;
	char eName[20];
	float sal;
	struct Employee *next;
}Emp;

void aD(Emp *ptr){
	printf("%d\n",ptr->eId);
	printf("%s\n",ptr->eName);
	printf("%f\n",ptr->sal);
	printf("%p\n",ptr->next);

}

void main(){
	Emp *emp1 = (Emp*)malloc(sizeof(Emp));
	Emp *emp2 = (Emp*)malloc(sizeof(Emp));
	Emp *emp3 = (Emp*)malloc(sizeof(Emp));

	Emp *head = emp1;
	
	head->eId=1;
	strcpy(head->eName,"Kanha");
	head->sal=60.00;
	head->next=emp2;
	
	head->next->eId=2;
	strcpy(head->next->eName,"Rahul");
	head->next->sal=80.00;
	head->next->next=emp3;
	
	head->next->next->eId=3;
	strcpy(head->next->next->eName,"Ashish");
	head->next->next->sal=85.00;
	head->next->next->next=NULL;
	
	aD(emp1);
	aD(emp2);
	aD(emp3);

}
