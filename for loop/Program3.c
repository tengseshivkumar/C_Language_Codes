/*3. WAP to print all even numbers in reverse order and odd numbers in the standard
way. Both separately. Within a range.
*/

#include<stdio.h>

void main(){
	int x,y,i,j;
	printf("Enter start val::");
	scanf("%d",&x);
	printf("Enter end val::");
	scanf("%d",&y);

	printf("Even numbersi\n");
	for(i=y;i>=x;i--){
		if(i%2==0){
			printf("%d\n",i);
		}
	}

	printf("Odd Numbers\n");
	for(j=x;j<=y;j++){
		if(j%2!=0){
			printf("%d\n",j);
		}
	}


}
